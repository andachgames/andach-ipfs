@extends('template')

@section('css')
<link href="/css/player.css" rel="stylesheet">
@endsection

@section('content')
	<h1>Play {{ $file->name }}</h1>
	@if ($file->isDownloaded())
		<p>File is downloaded successfully <b>{{ $file->web_path }}</b>. Play below.</p>
		<h2 class="title">Single Song</h2>
		<div class="row">
			<div class="col-12">
				<div id="single-song-player">
					<img amplitude-song-info="cover_art_url" amplitude-main-song-info="true" />
					<div class="bottom-container">
						<progress class="amplitude-song-played-progress" amplitude-main-song-played-progress="true" id="song-played-progress"></progress>
						<div class="time-container">
							<span class="current-time">
							<span class="amplitude-current-minutes" amplitude-main-current-minutes="true"></span>:<span class="amplitude-current-seconds" amplitude-main-current-seconds="true"></span>
							</span>
							<span class="duration">
							<span class="amplitude-duration-minutes" amplitude-main-duration-minutes="true"></span>:<span class="amplitude-duration-seconds" amplitude-main-duration-seconds="true"></span>
							</span>
						</div>
						<div class="control-container">
							<div class="amplitude-play-pause" amplitude-main-play-pause="true" id="play-pause"></div>
							<div class="meta-container">
								<span amplitude-song-info="name" amplitude-main-song-info="true" class="song-name"></span>
								<span amplitude-song-info="artist" amplitude-main-song-info="true"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@elseif ($file->exists())
		<p>This file is downloaded, but there is an error - this is most likely caused by the fact that the IPFS daemon isn't running. </p>
	@else
		<p>File is not downloaded. Please <a href="{{ route('files.download', $file->id) }}">click here</a> to download. </p>
	@endif
@endsection

@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/amplitudejs/3.3.1/amplitude.min.js"></script>
<script>
Amplitude.init({
	"songs": [
		{
			"name": "Song Name 1",
			"artist": "Artist Name",
			"album": "Album Name",
			"url": "{{ $file->web_path }}",
			"cover_art_url": "/cover/art/url.jpg"
		}
	]
});
</script>
@endsection