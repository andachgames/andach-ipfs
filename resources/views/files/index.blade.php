@extends('template')

@section('content')
	@foreach ($files as $file)
	<p><a href="{{ route('files.download', $file->id) }}">{{ $file->name }}</a> - <a href="{{ route('files.play', $file->id) }}">Play</a></p>
	@endforeach
@endsection