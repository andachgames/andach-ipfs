<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    protected $table = 'folders';

    public function children()
    {
    	return $this->hasMany('App\Folder', 'parent_id');
    }

    public function getFullPathAttribute()
    {
        if ($this->parent)
        {
            return $this->parent->full_path.'/'.$name;
        }

        return $this->name;
    }

    public function files()
    {
    	return $this->hasMany('App\File', 'folder_id');
    }

    public function parent()
    {
    	return $this->belongsTo('App\Folder', 'parent_id');
    }
}
