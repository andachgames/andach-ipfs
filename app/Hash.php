<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hash extends Model
{
    protected $table = 'hashes';

    public function file()
    {
    	return $this->belongsTo('App\File', 'file_id');
    }

    public function verifications()
    {
    	return $this->hasMany('App\HashVerification', 'hash_id');
    }

    public function verify()
    {
    	//TODO: This
    	return true;
    }
}
