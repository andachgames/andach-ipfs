<?php

namespace App;

use App\IPFS;
use Illuminate\Database\Eloquent\Model;
use Storage;

class File extends Model
{
    protected $fillable = ['name', 'filename', 'ipfs_hash'];
    protected $table = 'files';

    public function download()
    {
        $ipfs = new IPFS();
        Storage::put($this->full_path, $ipfs->get($this->ipfs_hash));
    }

    public function exists()
    {
        return Storage::exists($this->full_path);
    }

    public function filesDownloading()
    {
    	return $this->hasMany('App\FilesDownloading', 'file_id');
    }

    public function filesize()
    {
        if ($this->exists())
        {
            return Storage::size($this->full_path) > 0;
        }

        return false;
    }

    public function filetype()
    {
    	return $this->belongsTo('App\Filetype', 'filetype_id');
    }

    public function folder()
    {
    	return $this->belongsTo('App\Folder', 'folder_id');
    }

    public function getFullPathAttribute()
    {
        if ($this->folder)
        {
            return $this->folder->full_path.'/'.$this->filename;
        }
        return $this->filename;
    }

    public function getWebPathAttribute()
    {
        return '/storage/ipfs/'.$this->full_path;
    }

    public function hashes()
    {
    	return $this->hasMany('App\Hash', 'file_id');
    }

    public function isDownloaded()
    {
        return $this->filesize() > 0;
    }

    public function thumbnailFile()
    {
    	return $this->hasMany('App\File', 'thumbnail_file_id');
    }

    public function verify()
    {
        if (!$this->isDownloaded())
        {
            return false;
        }

        foreach ($this->hashes as $hash)
        {
            if (!$hash->verify())
            {
                return false;
            }
        }

        return true;
    }
}
