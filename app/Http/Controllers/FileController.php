<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;

class FileController extends Controller
{
	public function download($id)
	{
		$file = File::find($id);
		$file->download();

		return redirect()->route('files.index');
	}

    public function index()
    {
    	$files = File::all();
    	return view('files.index', ['files' => $files]);
    }

    public function play($id)
    {
    	$file = File::find($id);

    	return view('files.play', ['file' => $file]);
    }
}
