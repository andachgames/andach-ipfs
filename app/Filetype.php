<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filetype extends Model
{
    protected $table = 'filetypes';

    public function files()
    {
    	return $this->belongsTo('App\File', 'filetype_id');
    }
}
