<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HashVerification extends Model
{
    protected $table = 'hashes_verifications';

    public function file()
    {
    	return $this->belongsTo('App\File', 'file_id');
    }

    public function hash()
    {
    	return $this->belongsTo('App\Hash', 'hash_id');
    }
}
