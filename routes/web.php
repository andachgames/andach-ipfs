<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/files', 'FileController@index')->name('files.index');
Route::get('/files/{id}/download', 'FileController@download')->name('files.download');
Route::get('/files/{id}/play', 'FileController@play')->name('files.play');
Auth::routes();
