<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('filename');
            $table->string('ipfs_hash');
            $table->integer('size')->nullable();
            $table->integer('folder_id')->nullable();
            $table->integer('thumbnail_file_id')->nullable();
            $table->integer('filetype_id')->nullable();
            $table->boolean('is_downloaded')->nullable();
            $table->decimal('downloaded_percentage', 8, 2)->nullable();
            $table->datetime('downloaded_checked_on')->nullable();
            $table->string('downloaded_filename')->nullable();
            $table->integer('downloaded_folder_id')->nullable();
            $table->timestamps();
        });

        Schema::create('filetypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('extension');
            $table->timestamps();
        });

        Schema::create('folders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('parent_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
        Schema::dropIfExists('filetypes');
        Schema::dropIfExists('folders');
    }
}
