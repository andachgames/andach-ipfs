<?php

use App\File;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Need to take all CSV files and load them into the files directory. 
        $files = Storage::disk('csv')->allFiles();

        foreach ($files as $filename)
        {
        	$path = storage_path('csv\\'.$filename);
        	$file = fopen($path, 'r');

        	while (($data = fgetcsv($file)) !==FALSE)
			{
				$array['name'] = $data[0];
				$array['filename'] = $data[1];
				$array['ipfs_hash'] = $data[2];

				$putfile = new File($array);
				$putfile->save();
			}
        }
    }
}
